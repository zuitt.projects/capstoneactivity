-- Return the customerName of the customers who are from the Philippines

SELECT * FROM customers WHERE country = "Philippines";

-- Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"

SELECT contactLastname, contactFirstName FROM customers where customerName = "La Rochelle Gifts";

-- Return the product name and MSRP of the product named "The Titanic";

SELECT productName, MSRP from products WHERE productName = "The Titanic";

-- Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- Return the names of customers who have no registered state

SELECT customerName FROM customers WHERE state IS NULL;

-- Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve

SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > "3000";

-- Return the customer numbers of orders whose comments contain the string 'DHL'

SELECT orderNumber from orders WHERE comments LIKE "%DHL%";

-- Return the product lines whose text description mentions the phrase 'state of the art'

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- Return the countries of customers without duplication

SELECT DISTINCT country FROM customers;

-- Return the statuses of orders without duplication
SELECT DISTINCT status FROM orders;

-- Return the customer names and countries of customers whose country is USA, France, or Canada

SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

-- Return the first name, last name, and office's city of employees whose offices are in Tokyo

SELECT employees.firstName, employees.lastName, offices.city FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode WHERE offices.city = "Tokyo";


-- Return the customer names of customers who were served by the employee named "Leslie Thompson"

SELECT customers.customerName FROM customers WHERE salesRepEmployeeNumber = "1166";


-- Return the product names and customer name of products ordered by "Baane Mini Imports"

SELECT products.productName, customers.customerName FROM orders
    JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
	JOIN customers ON orders.customerNumber = customers.customerNumber
    JOIN products ON orderdetails.productCode = products.productCode WHERE orders.customerNumber = "121";

-- Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country

SELECT employees.firstName, employees.lastName, customerName, offices.country FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode
    JOIN customers ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE customers.country = offices.country;



-- Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000

SELECT products.productName, products.quantityInStock FROM products
	JOIN productlines ON products.productLine = productlines.productLine WHERE products.productLine = "planes" AND products.quantityInStock < 1000;

-- Return the customer's names with a phone number containing "+81".

SELECT customerName FROM customers WHERE PHONE LIKE "%+81%";